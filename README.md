# Angular Slider 

This is an image slider. 


## Requirements

    bower(NodeJS),
    angularjs in project

## Install

You can install this package either with  with bower.

    'bower install https://hmarikyan@bitbucket.org/hmarikyan/angular-slider.git'


## Documentation

Index.html

    <link rel="stylesheet" href="bower_components/angular-slider/slider.css">
    <script src="bower_components/angular-slider/slider.js"></script>
    
app.js

     var app = angular.module('app', ['slider']);
     
     app.controller('indexController', ['$scope', function ($scope) {
        $scope.items = [
            {
                "src": "images/6887285-images.jpg",
                "title": "Cows",
                "desc": "Look there are few cows on the field.."
            },
            {
                "src": "images/hero_dog_482206371.jpg",
                "title": "Hero Dog",
                "desc": "Funny dog :P"
            }
        ];
        
        $scope.on_change = function(index){
            console.log(index);
        }
    
    }]);
    
   
usage in templates

     <div my-slider="items" animation-type="linear" ng-width="1000px" ng-height="600px" ng-autoplay-delay="5000" ng-font="20px" on-change="on_change(index)"></div>