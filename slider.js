var slider = angular.module('slider', []);

/**
 *  SLIDER
 */
slider.directive('mySlider',['$timeout', function ($timeout) {
    return {
        restrict: "A",
        template:'<div class="slider-wrapper" ng-mouseover="show_controls = 1" ng-mouseout="show_controls = 0">' +
                    '<div class="slider-image-wrapper">' +
                        '<img class="slide" ng-class="hide_animation" ng-show="hide_animation" ng-src="{{ prev_item.src }}" />' +
                        '<img class="slide2" ng-class="show_animation" ng-src="{{ current_item.src }}"/>' +
                    '</div>' +
                    '<div class="slider-text">' +
                        '<div class="slider-title">{{ current_item.title }} </div>' +
                        '<div class="slider-description">{{ current_item.desc }} </div>' +
                    '</div>' +
                    '<div class="slider-next slider-control" ng-show="show_controls" ng-click="next()"></div>' +
                    '<div class="slider-prev slider-control" ng-show="show_controls" ng-click="prev()"></div>' +
                 '</div>',
        scope: {
            items: '=mySlider',
            on_change: '&onChange'
        },
        link: function (scope, el, attrs) {
            var wrapper = angular.element(el[0].querySelector('.slider-wrapper'));

            //promise
            var play_next = null;

            //size
            scope.width = attrs.ngWidth ? attrs.ngWidth : '400px';
            scope.height = attrs.ngHeight ? attrs.ngHeight : '250px';
            scope.font = attrs.ngFont ? attrs.ngFont : '12px';

            wrapper.css({
                width: scope.width,
                height: scope.height,
                'font-size': scope.font
            });

            //styles
            scope.anim_type = attrs.animationType;
            if(scope.anim_type == 'linear'){
                scope.hide_animation_type = 'animate-hide-linear';
                scope.hide_animation_type2 = 'animate-hide2-linear';
                scope.show_animation_type = 'animate-show-linear';
                scope.show_animation_type2 = 'animate-show2-linear';
            }else if(scope.anim_type == 'rotate'){
                scope.hide_animation_type = 'animate-hide-rotate';
                scope.hide_animation_type2 = 'animate-hide2-rotate';
                scope.show_animation_type = 'animate-show-rotate';
                scope.show_animation_type2 = 'animate-show2-rotate';
            }else if(scope.anim_type == 'flip'){
                scope.hide_animation_type = 'animate-hide-flip';
                scope.hide_animation_type2 = 'animate-hide2-flip';
                scope.show_animation_type = 'animate-show-flip';
                scope.show_animation_type2 = 'animate-show2-flip';
            }

            scope.hide_animation = null;
            scope.show_animation = null;

            //set current class for image changing
            var set_class = function(show, hide) {
                if(hide == 1){
                    scope.hide_animation = scope.hide_animation_type;
                }else if(hide == 2){
                    scope.hide_animation = scope.hide_animation_type2;
                }else{
                    scope.hide_animation = "";
                }

                if(show == 1){
                    scope.show_animation = scope.show_animation_type;
                }else if(show == 2){
                    scope.show_animation = scope.show_animation_type2;
                }else{
                    scope.show_animation = "";
                }
            };

            //init items/indexes
            scope.prev_show = false;
            scope.current_show = false;

            scope.current_item_index = 0;
            scope.current_item = scope.items[scope.current_item_index];
            scope.prev_item = scope.items[scope.current_item_index];


            //helper function
            var set_item = function(animation){
                set_class(animation, animation);

                scope.prev_item = scope.current_item;
                scope.current_item = scope.items[scope.current_item_index];

                if(attrs.ngAutoplayDelay){
                    $timeout.cancel(play_next);
                    play_next = $timeout(function() { scope.next(); }, attrs.ngAutoplayDelay);
                }

                //callback
                if(typeof scope.on_change === 'function'){
                    scope.on_change({index: scope.current_item_index});
                }
            };

            //
            var reset_animation = function(){
                set_class(false, false);
            };

            //show next item
            scope.next = function () {
                if (scope.items[scope.current_item_index + 1]) {
                    scope.current_item_index++;
                } else {
                    scope.current_item_index = 0;
                }

                set_item(1);

                $timeout(reset_animation,500);
            };

            //show previous item
            scope.prev = function () {

                if (scope.items[scope.current_item_index - 1]) {
                    scope.current_item_index--;
                } else {
                    scope.current_item_index = scope.items.length - 1;
                }

                set_item(2);

                $timeout(reset_animation ,500);
            };

            //autopaly pics
            if(attrs.ngAutoplayDelay) {
                play_next = $timeout(function () {
                    scope.next();
                }, attrs.ngAutoplayDelay);
            }

        }
    }
}]);
